from typing import Any, Dict

from google.cloud import bigquery


class BigQueryHelper:
    def __init__(self, client: bigquery.Client):
        self.client = client

    def get_max_timestamp(self, dst_table: Dict[str, str], station: str = None):
        query = f"""
        SELECT MAX(heure_de_paris) as max_ts from `{dst_table['project']}.{dst_table['dataset']}.{dst_table['table']}`
        WHERE {'station_id = @stationId' if station else '1=1'}
        """
        job_config = bigquery.QueryJobConfig(
            query_parameters=([
                bigquery.ScalarQueryParameter("stationId", "STRING", station)
            ] if station else []))
        job = self.client.query(query, job_config=job_config)
        for row in job.result():  # only one expected row
            return row["max_ts"]
        return None  # no data

    def append_data(self, data: Dict[str, Any], dst_table: Dict[str, str]):
        query = f"""
        INSERT INTO `{dst_table['project']}.{dst_table['dataset']}.{dst_table['table']}` (
            id, station_id, timestamp, data, humidite, direction_du_vecteur_de_vent_max, pluie_intensite_max,
            pression, direction_du_vecteur_vent_moyen, type_de_station, pluie, direction_du_vecteur_de_vent_max_en_degres,
            force_moyenne_du_vecteur_vent, force_rafale_max, temperature_en_degre_c, heure_de_paris, heure_utc
        )
        VALUES (
            '{data.get('id','')}',
            '{data.get('station_id','')}',
            '{data.get('timestamp','')}',
            '{data['fields'].get('data','')}',
            {data['fields'].get('humidite',0)},
            {data['fields'].get('direction_du_vecteur_de_vent_max',0)},
            {data['fields'].get('pluie_intensite_max',0.0)},
            {data['fields'].get('pression',0)},
            {data['fields'].get('direction_du_vecteur_vent_moyen',0)},
            '{data['fields'].get('type_de_station','')}',
            {data['fields'].get('pluie',0)},
            {data['fields'].get('direction_du_vecteur_de_vent_max_en_degres',0.0)},
            {data['fields'].get('force_moyenne_du_vecteur_vent',0)},
            {data['fields'].get('force_rafale_max',0)},
            {data['fields'].get('temperature_en_degre_c',0.0)},
            '{data['fields'].get('heure_de_paris','')}',
            '{data['fields'].get('heure_utc','')}'
        );
        """
        query_job = self.client.query(query)
