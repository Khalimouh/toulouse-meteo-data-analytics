resource "google_storage_bucket" "backfill_bucket" {
  project  = var.project
  name     = "sl-tmda-backfill"
  location = var.region

  uniform_bucket_level_access = true
  force_destroy               = true

  depends_on = [google_project_service.apis]
}
