#Rapport et proposition de solutions

## KeyError lors de l'insertion dans BQ:
Le code du cloud run tombe en erreur KeyError: 'type_de_station' at .append_data ( /code/./bigquery_helper.py:41 ) car la clé 'type_de_station' n'est pas présente dans le dictionnaire dans certains cas.
Pour éviter l'apparition de cette exception, il faut remplacer l'operateur [] de la classe dict par la methode .get("key", value) qui récupére la valeur correspondant à la key ou une valeur par defaut (value) si la clé n'existe pas.


## Axes d'optimisations de schema et de requetes:
Il serait pertinant de faire évoluer le schema de la table de fait ajouter les notions de partitionement et de clustering. Au vu des requetes utilisé dans le code python et dans le workload de la partie de 2, je propose
un partitionement sur la colonne heure_de_paris et un clustering sur le station_id.
Cette approche permettra d'améliorer les performances en insertion et selection sur la table de fait.
Il serait aussi interresant d'inclure la dimension station dans la table de fait comme un champ imbriqué (remplacer les JOIN par des UNNEST).
A noter que cette solution implique un compromis entre la taille de la table et la vitesse d'execution des requetes.
Une idee pourrait être de créer une vue matérialisée sur les requetes les plus utilisés ou aggréger les données d'une façon à simplifier le workload de requetes.

Je vois aussi que chaque données de station est écrite via un appel individuel. Il serait pertinant de le remplacer par un bulk insert en utilisant pandas et client.load_table_from_dataframe

## Proposition d'amélioration de la maintenabilité et l'ingeston:
Une premiere proposition serait de modifier la façon dont sont faites les requetes à l'API meteo. Dans la version actuelle du code, on boucle sur toutes les stations et on fait un appel api pour récuprer les données de telemetrie.
Il serait interresant de tirer parti de "concurrency" que propose python en utilisant un modele asynchrone ou n requetes avec n le nombre de stations seraient effecutées *concurently* plutot que de façon synchrone. On pourrait utiliser pour cela le module
asyncio. Cela reduirait le temps de traitement de cette partie du code.
On pourrait ensuite penser à traiter en parrallele les données telemetriques pour finir par faire une union dans un unique dataframe qui sera inséré en bulk insert dans bigquery.

Une autre approche qui cette fois implique un changement dans l'architecture du process d'ingestion serait d'envisager un modele asynchrone basé sur Cloud Pub/Sub.
Plutot que d'avoir un seul Cloud Run qui centralise les operations de collecte, traitement et ingestion. On pourrait envisager un premier Cloud Run qui sera toujours déclenché par Cloud Scheduler toutes les 15 minutes mais qui n'aura comme objectif que
d'aller chercher via une requete les données de telemetrie et de les écrire dans un topic. Il suffit ensuite de creer une souscription à second cloud run qui aura pour role de traiter les données de télémetrie et de faire l'insertion ou même d'écrire dans une
souscription bigquery qui s'occupera de faire l'insertion.

L'avantage de cette approche est double, d'une part on a une reparation des responsabilité, les operation de requetages d'api et de traitement et insertion sont séparés dans des bases de codes différentes ce qui améliore la maintenabilité.
De plus, le modele Pub/Sub permet de faire un scaling plus interessant des différents composant ou même d'envisager de remplacer Cloud Run par du compute enigne ou du GKE.

Pour ce qui est du scaling, je propose aussi comme optimisation de faire un scaling horizontal du Cloud Run en modifiant le paramètre "autoscaling.knative.dev/maxScale" = "1" dans la conf terraform et d'augementer les performances de l'instance Cloud Run (Scaling Vertical)
