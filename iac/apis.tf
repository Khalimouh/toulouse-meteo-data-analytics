

resource "google_project_service" "apis" {
  for_each = toset(["containerregistry", "run", "cloudscheduler"])
  project = var.project
  service = "${each.key}.googleapis.com"
}
