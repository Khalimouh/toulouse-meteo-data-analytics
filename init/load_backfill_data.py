from google.cloud import bigquery

PROJECT_ID = "toulouse-meteo-data-analytics"
BUCKET_NAME = "sl-tmda-backfill"
DST_TABLE = {
    "project": "toulouse-meteo-data-analytics",
    "dataset_id": "ds_toulouse_meteo_data",
    "table_id": "tb_telemetry",
}


def run():
    bq = bigquery.Client(project=PROJECT_ID)

    external_table_ref = f"{DST_TABLE['project']}.{DST_TABLE['dataset_id']}.{DST_TABLE['table_id']}_tmp"
    tmp_table = bigquery.Table(external_table_ref)
    tmp_table.external_data_configuration = bigquery.table.ExternalConfig.from_api_repr({
        "sourceFormat": "NEWLINE_DELIMITED_JSON",
        "autodetect": True,
        "sourceUris": [f"gs://{BUCKET_NAME}/*.jsonl"]
    })
    tmp_table = bq.create_table(tmp_table)  # Make an API request.

    bq.query(
        query=f"SELECT regexp_extract(_FILE_NAME, r'/([\w-]+).jsonl') as station_id, * FROM `{external_table_ref}`",
        location="europe-west1",
        job_config=bigquery.job.QueryJobConfig(
            destination=f"{DST_TABLE['project']}.{DST_TABLE['dataset_id']}.{DST_TABLE['table_id']}",
            write_disposition="WRITE_EMPTY",
            create_disposition="CREATE_IF_NEEDED",
        )
    ).result()

    bq.delete_table(tmp_table)


if __name__ == '__main__':
    run()
