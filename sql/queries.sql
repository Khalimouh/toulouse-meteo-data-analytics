--1- Combien y a-t-il d’enregistrements en 2021 ?
SELECT COUNT(*)
FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry`
WHERE EXTRACT(YEAR FROM heure_de_paris) = 2021

--2- Pourcentage de stations qui ont été mise à jour hier :
-- Une station mise à jour pour une date donnée est une station qui reçoit une ou plusieures mesures pour cette date
-- Current_Date = 2024-03-09
WITH count_all_stations AS (
  SELECT count(distinct t.station_id) as count_total
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
),
count_updated AS (
  SELECT count(distinct t.station_id) as count_updated
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
  WHERE EXTRACT(DATE FROM t.heure_de_paris) = CURRENT_DATE() - 1
)
SELECT (updated.count_updated / all_station.count_total) * 100 as updated_ratio
FROM count_all_stations all_station, count_updated updated


--3- Liste des stations (titre, description) dont la pression max en 2021 a été inférieure à 100000:
WITH max_pression_2021 AS (
  SELECT s.title, s.description, max(t.pression) as max_pression
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_stations` s
  RIGHT JOIN `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t ON s.slug = t.station_id
  WHERE EXTRACT(YEAR FROM heure_de_paris) = 2021
  GROUP BY s.title, s.description
) SELECT title, description
FROM max_pression_2021
WHERE max_pression < 100000;

--4- Liste des station_id de la table télémétries (tb_telemetry) qui ne sont pas référencés dans la table référentiel des stations (tb_stations)
SELECT DISTINCT t.station_id
FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
EXCEPT DISTINCT
SELECT DISTINCT s.slug
FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_stations` s;

--5- Moyenne de la température par station la semaine dernière calendaire
-- A la date du 2024-02-03 je suppose que la derniere semaine calendaire est entre le lundi 2023-02-26 et 2023-03-03
WITH temp_current_week AS (
  SELECT t.station_id, t.temperature_en_degre_c
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
  WHERE EXTRACT(date from t.heure_de_paris) BETWEEN DATE_SUB(LAST_DAY(CURRENT_DATE(), WEEK), INTERVAL 12 DAY) AND DATE_SUB(LAST_DAY(CURRENT_DATE(), WEEK), INTERVAL 6 DAY)
)
SELECT station_id, avg(temperature_en_degre_c) as avg_temp -- Peut être faire un ROUND(,2) ou un CAST( as INT64) ?
FROM temp_current_week
GROUP BY station_id

--6- A quelle heure la pression atmosphérique est-elle au minimum en général ?
-- Hypotèse: a quelle heure en général la pression athmosthèrique est elle au minimum de la préssion (all time / stations)
WITH min_athmospherical_pressure AS (
  SELECT min(t.pression) as min_pression
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
),
extracted_hours AS (
  SELECT EXTRACT(TIME FROM t.heure_de_paris) as hour
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
  INNER JOIN min_athmospherical_pressure map ON t.pression = map.min_pression
),
count_hours AS (
  SELECT hour, count(*) as occurence
  FROM extracted_hours
  WHERE hour IS NOT NULL
  GROUP BY hour
)
SELECT hour
FROM count_hours
ORDER BY occurence DESC
LIMIT 1;

--7- Temps moyen entre 2 mesures par station
WITH lagged_metrics AS (
  SELECT t.station_id, t.heure_de_paris, LAG(t.heure_de_paris) OVER (PARTITION BY t.station_id ORDER BY heure_de_paris ASC) AS previous
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
  WHERE t.heure_de_paris IS NOT NULL
  ORDER BY t.heure_de_paris ASC
),
diff_per_metric AS (
  SELECT station_id, TIMESTAMP_DIFF(heure_de_paris, previous, MINUTE) AS diff -- Comment afficher la difference: minutes, secondes etc... ?
  FROM lagged_metrics
)
SELECT station_id, avg(diff) as avg_diff_per_station_in_minutes
FROM diff_per_metric
GROUP BY station_id
ORDER BY station_id ASC

--8- Donner la moyenne de la température, le premier mois d’exportation pour chaque station
-- Je suppose ici que la période de temps se situe entre le date du premier enregistrement et la date du premier enregistrement + 30 jours
WITH first_date_per_station AS (
  SELECT station_id, min(t.heure_de_paris) as date_debut
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
  GROUP BY station_id
),
data_join_period AS (
  SELECT t.station_id,t.heure_de_paris,t.temperature_en_degre_c,date_debut, DATE_ADD(date_debut, INTERVAL 30 DAY) as date_fin_mois
  FROM first_date_per_station
  INNER JOIN `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t ON first_date_per_station.station_id = t.station_id
)
SELECT station_id, avg(temperature_en_degre_c) as average_temperature_first_month
FROM data_join_period
WHERE heure_de_paris BETWEEN date_debut AND date_fin_mois
GROUP BY station_id
ORDER BY station_id DESC;

-- Dans le cas ou le premier mois correspond à la fin du mois calendaire
WITH first_date_per_station AS (
  SELECT station_id, min(t.heure_de_paris) as date_debut
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
  GROUP BY station_id
),
data_join_period AS (
  SELECT t.station_id,t.heure_de_paris,t.temperature_en_degre_c,f.date_debut, TIMESTAMP(LAST_DAY(DATETIME(f.date_debut), MONTH)) as date_fin_mois
  FROM first_date_per_station f
  INNER JOIN `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t ON f.station_id = t.station_id
)
SELECT station_id, avg(temperature_en_degre_c) as average_temperature_first_month
FROM data_join_period
WHERE heure_de_paris BETWEEN date_debut AND date_fin_mois
GROUP BY station_id
ORDER BY station_id DESC;


--9- Donner la somme cumulée de la pluie sur une fenêtre glissante de 3 jours (précédents), pour chaque jours et chaque station de la semaine n°5 de l’année 2021
-- Je suppose ici que la fenetre glissante est de taille trois avec le jour n inclus donc (n, n-1 et n-2)
-- Si ce n'est pas le cas ajuster les paramètres a et b dans la clause ROWS BETWEEN a PRECEDIND ANDd b).
WITH week5_data AS (
  SELECT t.station_id, EXTRACT(DATE FROM t.heure_de_paris) as day, t.pluie
  FROM `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` t
  WHERE EXTRACT(DATE FROM t.heure_de_paris) BETWEEN "2021-02-01" AND "2021-02-07"
),
rain_per_day AS (
  SELECT station_id, day, round(sum(pluie),2) as pluie
  FROM week5_data
  GROUP BY station_id, day
)
SELECT *, SUM(pluie) OVER (PARTITION BY station_id ORDER BY day ROWS BETWEEN 2 PRECEDING AND CURRENT ROW) as sum
FROM rain_per_day
ORDER BY station_id ASC, day ASC

--10- Donner la liste des plages de jours consécutifs de canicules (température supérieur à 25 degré toute la journée) pour chacune des stations
